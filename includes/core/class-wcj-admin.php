<?php
/**
 * SIP panel admin menu class.
 *
 * @since 3.2.4
 * @version 5.3.0
 * @package sip_sequential_order_numbers_woocommerce
 * @author  ShopitPress
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'WCJ_Admin' ) ) :

define( 'SIP_SON_UTM_CAMPAIGN', 'sip-sequential-order-numbers' );
define( 'SIP_SON_ADMIN_VERSION' , '1.0.5' );

if ( ! defined( 'SIP_SPWC_PLUGIN' ) )
  define( 'SIP_SPWC_PLUGIN',  'SIP Social Proof for WooCommerce' );

if ( ! defined( 'SIP_FEBWC_PLUGIN' ) )
  define( 'SIP_FEBWC_PLUGIN', 'SIP Front End Bundler for WooCommerce' );

if ( ! defined( 'SIP_RSWC_PLUGIN' ) )
  define( 'SIP_RSWC_PLUGIN',  'SIP Reviews Shortcode for WooCommerce' );

if ( ! defined( 'SIP_SON_PLUGIN' ) )
  define( 'SIP_SON_PLUGIN',  'SIP Sequnetial Order Numbers for WooCommerce' );

if ( ! defined( 'SIP_CCWC_PLUGIN' ) )
  define( 'SIP_CCWC_PLUGIN',  'SIP Cookie Check for WooCommerce' );

if ( ! defined( 'SIP_AENWC_PLUGIN' ) )
  define( 'SIP_AENWC_PLUGIN',  'Sip Advanced Email Notification For Woocommerce' );

if ( ! defined( 'SIP_WPGUMBY_THEME' ) )
  define( 'SIP_WPGUMBY_THEME','WPGumby' );

if ( ! defined( 'SIP_SPWC_PLUGIN_URL' ) )
  define( 'SIP_SPWC_PLUGIN_URL',  'https://shopitpress.com/plugins/sip-social-proof-woocommerce/' );

if ( ! defined( 'SIP_FEBWC_PLUGIN_URL' ) )
  define( 'SIP_FEBWC_PLUGIN_URL', 'https://shopitpress.com/plugins/sip-front-end-bundler-woocommerce/' );

if ( ! defined( 'SIP_AENWC_PLUGIN_URL' ) )
  define( 'SIP_AENWC_PLUGIN_URL',  'https://shopitpress.com/plugins/sip-advanced-email-notifications-for-woocommerce/' );

if ( ! defined( 'SIP_RSWC_PLUGIN_URL' ) )
  define( 'SIP_RSWC_PLUGIN_URL',  'https://shopitpress.com/plugins/sip-reviews-shortcode-woocommerce/' );

if ( ! defined( 'SIP_SON_PLUGIN_URL' ) )
  define( 'SIP_SON_PLUGIN_URL',  'https://shopitpress.com/plugins/sip-reviews-shortcode-woocommerce/' );

if ( ! defined( 'SIP_WPGUMBY_THEME_URL' ) )
  define( 'SIP_WPGUMBY_THEME_URL','https://shopitpress.com/themes/wpgumby/' );

if ( ! defined( 'SIP_CCWC_PLUGIN_URL' ) )
  define( 'SIP_CCWC_PLUGIN_URL',  'https://shopitpress.com/plugins/sip-cookie-check-woocommerce/' );

$get_optio_version = get_option( 'sip_version_value' );
if( $get_optio_version == "" ) {
  add_option( 'sip_version_value', SIP_SON_ADMIN_VERSION );
}
if ( version_compare( SIP_SON_ADMIN_VERSION , $get_optio_version , ">=" ) ) {
  update_option( 'sip_version_value', SIP_SON_ADMIN_VERSION );
}

class WCJ_Admin {

	/**
	 * Constructor.
	 *
	 * @version 5.3.0
	 * @since   3.2.4
	 */
	function __construct() {
		if ( is_admin() ) {
			add_filter( 'booster_message',                                           'wcj_get_plus_message', 100, 3 );

			if ( apply_filters( 'wcj_can_create_admin_interface', true ) ) {
				add_action( 'admin_menu', array( $this, 'sip_son_admin_menu' ) );
			    add_action( 'admin_menu', array( $this, 'sip_son_config_menu' ), 20 );
			    add_action( 'admin_menu', array( $this, 'sip_son_sip_extras_admin_menu' ), 2000 );
				add_filter( 'woocommerce_get_settings_pages',                            array( $this, 'add_wcj_settings_tab' ), 1 );
				add_filter( 'plugin_action_links_' . plugin_basename( WCJ_PLUGIN_FILE ), array( $this, 'action_links' ) );
				// add_action( 'admin_menu',                                                array( $this, 'booster_menu' ), 100 );
				add_filter( 'admin_footer_text',                                         array( $this, 'admin_footer_text' ), 2 );
				if ( 'woocommerce-jetpack.php' === basename( WCJ_PLUGIN_FILE ) ) {
					add_action( 'admin_notices',                                         array( $this, 'check_plus_version' ) );
				}
			}
		}
	}

	/**
	 * check_plus_version.
	 *
	 * @version 3.4.0
	 * @since   2.5.9
	 * @todo    (maybe) use `wcj_is_plugin_active_by_file()`
	 * @todo    (maybe) expand "Please upgrade ..." message
	 */
	function check_plus_version() {
		$is_deprecated_plus_active = false;
		foreach ( wcj_get_active_plugins() as $active_plugin ) {
			$active_plugin = explode( '/', $active_plugin );
			if ( isset( $active_plugin[1] ) ) {
				if ( 'booster-plus-for-woocommerce.php' === $active_plugin[1] ) {
					return;
				} elseif ( 'woocommerce-jetpack-plus.php' === $active_plugin[1] || 'woocommerce-booster-plus.php' === $active_plugin[1] ) {
					$is_deprecated_plus_active = true; // can't `brake` because of possible active `booster-plus-for-woocommerce.php`
				}
			}
		}
		if ( $is_deprecated_plus_active ) {
			$class   = 'notice notice-error';
			$message = __( 'Please update <strong>Booster Plus for WooCommerce</strong> plugin.', 'woocommerce-jetpack' ) . ' ' .
				sprintf(
					__( 'Visit <a target="_blank" href="%s">your account page</a> on booster.io to download the latest Booster Plus version.', 'woocommerce-jetpack' ),
					'https://booster.io/my-account/?utm_source=plus_update'
				) . ' ' .
				sprintf(
					__( 'Click <a target="_blank" href="%s">here</a> for more info.', 'woocommerce-jetpack' ),
					'https://booster.io/booster-plus-for-woocommerce-update/'
				);
			echo '<div class="' . $class . '"><p>' . $message . '</p></div>';
		}
	}

	/**
	 * admin_footer_text
	 *
	 * @version 2.9.0
	 */
	function admin_footer_text( $footer_text ) {
		if ( isset( $_GET['page'] ) ) {
			if ( 'wcj-tools' === $_GET['page'] || ( 'wc-settings' === $_GET['page'] && isset( $_GET['tab'] ) && 'jetpack' === $_GET['tab'] ) ) {
				$rocket_icons = wcj_get_5_rocket_image();
				$rating_link = '<a href="https://wordpress.org/support/plugin/woocommerce-jetpack/reviews/?rate=5#new-post" target="_blank">' . $rocket_icons . '</a>';
				return sprintf(
					__( 'If you like <strong>Booster for WooCommerce</strong> please leave us a %s rating. Thank you, we couldn\'t have done it without you!', 'woocommerce-jetpack' ),
					$rating_link
				);
			}
		}
		return $footer_text;
	}

	/**
	 * Add menu item
	 *
	 * @version 3.5.3
	 */
	function booster_menu() {
		// add_submenu_page(
		// 	'woocommerce',
		// 	__( 'Booster for WooCommerce', 'woocommerce-jetpack' ),
		// 	__( 'SIP Sequential Order Numbers', 'woocommerce-jetpack' ) ,
		// 	( 'yes' === wcj_get_option( 'wcj_' . 'admin_tools' . '_enabled', 'no' ) && 'yes' === wcj_get_option( 'wcj_admin_tools_show_menus_to_admin_only', 'no' ) ? 'manage_options' : 'manage_woocommerce' ),
		// 	'admin.php?page=wc-settings&tab=jetpack&wcj-cat=shipping_and_orders&section=order_numbers'
		// );
	}

		/**
   * Registers the admin menu for managing the ShopitPress options.
   *
   * @since 1.0.0
   */
  public function sip_son_admin_menu() {
    $icon_svg = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiIHdpZHRoPSI0MHB4IiBoZWlnaHQ9IjMycHgiIHZpZXdCb3g9IjAgNTAgNzI1IDQ3MCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNzI1IDQ3MCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+PGc+PHBhdGggZmlsbD0iI0ZGRkZGRiIgZD0iTTY0MC4zMjEsNDguNTk4YzI4LjU0LDAsNDMuNzI5LDI5Ljc5MiwzMi4xNzIsNTUuMTU4bC03Ni40MTYsMTY2Ljk1NGMtMTIuMDMyLTMyLjM0Ni01MC41NjUtNTUuNzU3LTg3LjktNjkuMTczYy00OC44NjItMTcuNjAyLTEyNy44NDMtMjEuODE5LTE5MC4wOTQtMzAuMzc5Yy0zNC4zMjEtNC42NjEtMTEwLjExOC0xMi43NS05Ny43OC01My4xMTVjMTMuMjM5LTQzLjA3NCw5Ni40ODEtNDcuNTkxLDEzMy44OC00Ny41OTFjODYuMTI5LDAsMTYwLjk1NCwxOS43NzEsMTYwLjk1NCw4My44NjZoOTkuNzQxVjQ4LjU5OEg2NDAuMzIxeiBNNTQzLjc5NiwxMDUuNTk0Yy03LjEwNS0yNy40NTgtMzIuMjc3LTQ4LjcxNy01OS4xNjktNTYuOTk3aDgyLjc3NkM1NjYuMjgxLDY2LjYxMyw1NTUuNDQ4LDk0LjE4MSw1NDMuNzk2LDEwNS41OTRMNTQzLjc5NiwxMDUuNTk0eiBNNTUwLjY0MSwzNzAuMTIzbC0xMy42MTEsMjkuNzIzYy02LjAzOCwxMy4yNzktMTkuMzI3LDIxLjYzNS0zMy45MjcsMjEuNjM1SDIyMS45NjljLTE0LjY2NiwwLTI3Ljk1NS04LjM1NS0zNC4wMDMtMjEuNjM1bC0xNS44NDQtMzQuNzIzYzEwLjkxMiwxNC43NDgsMjkuMzMxLDIzLjA4LDQ5LjA5OCwyOC4yODFDMzEzLjE1LDQxNy43MzIsNDY4LjUzNSw0MjEuNDgsNTUwLjY0MSwzNzAuMTIzTDU1MC42NDEsMzcwLjEyM3ogTTE2My43NjEsMzQ2Ljk5bC01OC4xNi0xMjcuMjQzYzE0LjY0MSwxNS42NTUsMzcuNjAxLDI3LjM2LDY2LjcyNCwzNi4yOTdjODUuNDA5LDI2LjI0MiwyMTMuODI1LDIyLjIyOSwyOTYuMjU0LDM1LjExN2M0MS45NDksNi41NjEsNDMuODU3LDQ3LjA4OCwxMy4yODksNjEuOTQ3Yy01Mi4zMzQsMjUuNTA2LTEzNS4yNDUsMjUuMzU5LTE5NC45NTcsMTEuNjk1QzIzNy4yMTksMjg1LjI1LDE1NS44MTksMzA0LjQ5LDE2My43NjEsMzQ2Ljk5TDE2My43NjEsMzQ2Ljk5eiBNODUuODY4LDE3Ni42OTJsLTMzLjM0Ni03Mi45MzdDNDAuOTQ5LDc4LjM5LDU2LjEzMSw0OC41OTgsODQuNjY5LDQ4LjU5OGgxMzYuOTY2QzE1OS43NTEsNjYuMTU0LDc3LjEwNSwxMTAuNjcsODUuODY4LDE3Ni42OTJMODUuODY4LDE3Ni42OTJ6Ii8+PHBhdGggZmlsbD0iI0ZGRkZGRiIgZD0iTTM2Mi41MywwLjA4NmgyNzcuNzkyYzYzLjk2NiwwLDEwMi4xODUsNjYuNzk1LDc2LjEzNSwxMjMuNzI2TDU4MS4wMzEsNDE5Ljk4NEM1NjcuMTQ3LDQ1MC4yODEsNTM2LjQzNSw0NzAsNTAzLjEwMyw0NzBIMzYyLjUzSDIyMS44OTJjLTMzLjM0NSwwLTY0LjA0My0xOS43MTktNzcuOTE3LTUwLjAxNkw4LjUzNSwxMjMuODEyQy0xNy40OTMsNjYuODgyLDIwLjY5MywwLjA4Niw4NC42NjksMC4wODZIMzYyLjUzeiBNMzYyLjUzLDIzLjk0Mkg4NC42NjljLTQ2LjIxOCwwLTczLjU2OCw0OC4yNjYtNTQuNDMsOTAuMDExbDEzNS4zNjIsMjk2LjA3OGMxMC4wNzIsMjEuOTYxLDMyLjIyNSwzNi4xMDUsNTYuMjkxLDM2LjEwNUgzNjIuNTNoMTQwLjU3M2MyNC4wNjcsMCw0Ni4yMTktMTQuMTQ1LDU2LjI3Ny0zNi4xMDVsMTM1LjM4Ni0yOTYuMDc4YzE5LjE0LTQxLjc0NS04LjIyNi05MC4wMTEtNTQuNDQ0LTkwLjAxMUgzNjIuNTN6Ii8+PC9nPjwvc3ZnPg==';
    //add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
    $this->hook = add_menu_page(
        __( 'SIP Plugin Panel', 'sip_plugin_panel' ),
        __( 'SIP Plugins', 'sip_plugin_panel' ),
        'manage_options',
        'sip_plugin_panel',
        NULL,
        $icon_svg,
        62.25
    );

    // Load global assets if the hook is successful.
    if ( $this->hook ) {
      // Enqueue custom styles and scripts.
      add_action( 'admin_enqueue_scripts',  array( $this, 'sip_son_admin_tab_style' ) );
    }
  }

  	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
  public function sip_son_admin_tab_style() {
    wp_register_style( 'sip_son_custom_css', esc_url( WCJ_PLUGIN_FILE .   '/admin/assets/css/custom.css', false, '1.0.0' ) );
    wp_enqueue_style( 'sip_son_custom_css' );
  }


  	public function sip_son_config_menu() {
    global $parent;
    $args = array(
          'create_menu_page' => true,
          'parent_slug'   => '',
          'page_title'    => __( 'SIP Sequential Order number for WooCommerce', 'sip_plugin_panel' ),
          'menu_title'    => __( 'SIP Sequential Order number for WooCommerce', 'sip_plugin_panel' ),
          'capability'    => 'manage_options',
          'parent'        => '',
          'parent_page'   => 'sip_plugin_panel',
          'page'          => 'sip_plugin_panel',
    );
    $parent = $args['parent_page'];

    if ( ! empty( $parent ) ) {
       add_submenu_page( $parent , 'SIP Sequential Order number for WooCommerce', 'SIP Sequential Order number for WooCommerce', 'manage_options', 'wc-settings&tab=jetpack&wcj-cat=shipping_and_orders&section=order_numbers', array( $this, 'sip_piw_settings_page_ui' ) );
      

    } else {
      add_menu_page( $args['page_title'], $args['menu_title'], $args['capability'], $args['page'], array( $this, 'sip_son_admin_menu_ui' ), NULL , 62.25 );

    }
    /* === Duplicate Items Hack === */
    $this->sip_son_remove_duplicate_submenu();

  }
  function create_tools_page() {

		// Tabs
		$tabs = apply_filters( 'wcj_tools_tabs', array(
			array(
				'id'    => 'dashboard',
				'title' => __( 'Tools Dashboard', 'woocommerce-jetpack' ),
			),
		) );
		$html = '<h2 class="nav-tab-wrapper woo-nav-tab-wrapper">';
		$active_tab = ( isset( $_GET['tab'] ) ) ? $_GET['tab'] : 'dashboard';
		foreach ( $tabs as $tab ) {
			$is_active = ( $active_tab === $tab['id'] ) ? 'nav-tab-active' : '';
			$html .= '<a href="' . add_query_arg( array( 'page' => 'wcj-tools', 'tab' => $tab['id'] ), get_admin_url() . 'admin.php' ) . '" class="nav-tab ' . $is_active . '">' . $tab['title'] . '</a>';
		}
		$html .= '</h2>';
		echo $html;

		// Content
		if ( 'dashboard' === $active_tab ) {
			$title = __( 'SIP Tools - Dashboard', 'woocommerce-jetpack' );
			$desc  = __( 'This dashboard lets you check statuses and short descriptions of all available Booster for WooCommerce tools. Tools can be enabled through WooCommerce > Settings > Booster. Enabled tools will appear in the tabs menu above.', 'woocommerce-jetpack' );
			echo '<h3>' . $title . '</h3>';
			echo '<p>' .  $desc . '</p>';
			echo '<table class="widefat" style="width:90%;">';
			echo '<tr>';
			echo '<th style="width:20%;">' . __( 'Tool', 'woocommerce-jetpack' ) . '</th>';
			echo '<th style="width:20%;">' . __( 'Module', 'woocommerce-jetpack' ) . '</th>';
			echo '<th style="width:50%;">' . __( 'Description', 'woocommerce-jetpack' ) . '</th>';
			echo '<th style="width:10%;">' . __( 'Status', 'woocommerce-jetpack' ) . '</th>';
			echo '</tr>';
			do_action( 'wcj_tools_' . 'dashboard' );
			echo '</table>';
		} else {
			do_action( 'wcj_tools_' . $active_tab );
		}
	}

  /**
   * To avoide the duplication of ShopitPress Extras menue and run the latest sip panel
   *
   * @since 1.0.1
   */
  public function sip_son_sip_extras_admin_menu() {
    global $parent;
    $get_optio_version = get_option( 'sip_version_value' );

    if ( version_compare( $get_optio_version , SIP_SON_ADMIN_VERSION , "<=" ) ) {

      if ( ! defined( 'SIP_PANEL_EXTRAS' ) ) {
          define( 'SIP_PANEL_EXTRAS' , TRUE);
          add_submenu_page( $parent , 'SIP Tools', 'SIP Tools</span>', 'manage_options', 'wcj-tools' , array( $this, 'create_tools_page' ) );
          add_submenu_page( $parent , 'ShopitPress Extras', '<span style="color:#FF8080 ">ShopitPress Extras</span>', 'manage_options', 'sip-extras', array( $this, 'sip_son_admin_menu_ui' ) );
          add_action( 'admin_enqueue_scripts',  array( $this, 'sip_son_admin_assets' ) );
      }
    }
  }
   /**
	* Outputs the admin menu UI for handling and managing addons, themes and licenses.
	*
	* @since 1.0.0
	*/
	public function sip_son_admin_menu_ui() { ?>
		<div class="wrap">
	    <h2>Shopitpress extras</h2>
			<h2 class="nav-tab-wrapper">
				<a class="nav-tab<?php if ( !isset( $_GET['action'] ) ) echo ' nav-tab-active'; ?>" href="admin.php?page=sip-extras"><?php _e( 'Plugins', 'sip-social-proof' ); ?></a>
				<a class="nav-tab<?php if ( isset( $_GET['action'] ) && 'themes' == $_GET['action'] ) echo ' nav-tab-active'; ?>" href="admin.php?page=sip-extras&amp;action=themes"><?php _e( 'Themes', 'sip-social-proof' ); ?></a>
			</h2>
			<?php
			if ( ! isset( $_GET['action'] ) ) {
				include("ui/plugin.php");
			} elseif ( 'themes' == $_GET['action'] ) {
				include("ui/themes.php");
			}
			?>
		</div>
		<?php
	}

  /**
   * Loads assets for the settings page.
   *SIP_RSWC_URL
   * @since 1.0.0
   */
  public function sip_son_admin_assets() {

    wp_register_style( 'sip_son_layout', esc_url( WCJ_PLUGIN_FILE .   '/admin/assets/css/layout.css', false, '1.0.0' ) );
    wp_enqueue_style( 'sip_son_layout' );
  }

  public function sip_son_remove_duplicate_submenu() {
    /* === Duplicate Items Hack === */
    remove_submenu_page( 'sip_plugin_panel', 'sip_plugin_panel' );
  }

  	/**
   * After loding this function global page show the admin panel
   *
   * @since     1.0.0
   */
  public function sip_son_settings_page() { ?>

    <div class="sip-panel-wrapper wrap">
      <h2>SIP Reviews Shortcode WooCommerce</h2>
      <div class="sip-container">
        <h2 class="nav-tab-wrapper">
          <a class="nav-tab<?php if ( !isset( $_GET['action'] ) ) echo ' nav-tab-active'; ?>" href="admin.php?page=wc-settings&tab=jetpack&wcj-cat=shipping_and_orders&section=order_numbers"><?php _e( 'Settings', 'sip-reviews-shortcode' ); ?></a>
          <a class="nav-tab<?php if ( isset( $_GET['action'] ) && 'help' == $_GET['action'] ) echo ' nav-tab-active'; ?>" href="admin.php?page=sip-reviews-shortcode-settings&amp;action=help"><?php _e( 'Help', 'sip-reviews-shortcode' ); ?></a>
          <a class="nav-tab sip-nav-premium<?php if ( isset( $_GET['action'] ) && 'pro' == $_GET['action'] ) echo ' nav-tab-active'; ?>" href="admin.php?page=sip-reviews-shortcode-settings&amp;action=pro"><?php _e( 'Get PRO', 'sip-reviews-shortcode' ); ?></a>
        </h2>
        <?php
          if ( ! isset( $_GET['action'] ) ) {
            sip_son_settings_page_ui();
          } elseif ( 'help' == $_GET['action'] ) {
            include( "admin/partials/ui/help.php" );
          } elseif ( 'pro' == $_GET['action'] ) {
            include( "admin/partials/ui/pro.php" );
          }
        ?>
      </div><!-- .container -->
    </div>

  <?php
  }

	/**
	 * Show action links on the plugin screen
	 *
	 * @version 5.2.0
	 * @param   mixed $links
	 * @return  array
	 */
	function action_links( $links ) {
		$custom_links = array(
			'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=jetpack&wcj-cat=shipping_and_orders&section=order_numbers' ) . '">' . __( 'Settings', 'woocommerce' ) . '</a>',
			'<a href="' . esc_url( 'https://booster.io/' ) . '">' . __( 'Docs', 'woocommerce-jetpack' ) . '</a>',
		);
		if ( 'woocommerce-jetpack.php' === basename( WCJ_PLUGIN_FILE ) ) {
			$custom_links[] = '<a target="_blank" href="' . esc_url( 'https://booster.io/plus/' ) . '">' . __( 'Unlock all', 'woocommerce-jetpack' ) . '</a>';
		} else {
			$custom_links[] = '<a target="_blank" href="' . esc_url( 'https://booster.io/my-account/booster-contact/' ) . '">' . __( 'Support', 'woocommerce-jetpack' ) . '</a>';
		}
		return array_merge( $custom_links, $links );
	}

	/**
	 * Add Jetpack settings tab to WooCommerce settings.
	 *
	 * @version 3.2.4
	 */
	function add_wcj_settings_tab( $settings ) {
		$_settings = include( WCJ_PLUGIN_PATH . '/includes/admin/class-wc-settings-jetpack.php' );
		$_settings->add_module_statuses( WCJ()->module_statuses );
		$settings[] = $_settings;
		return $settings;
	}

}

endif;

return new WCJ_Admin();
